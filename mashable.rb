require 'rubygems'
require 'rest_client'
require 'json'

#clase propia de la pagina MASHABLE
class Mashable
#Obtencion de la data del API de MASHABLE
  def mashable_api_request
    @url = 'http://mashable.com/stories.json'
    @response = RestClient.get(@url)
    @hash = JSON.parse(@response.body)
  end

  	#Organizacion, filtrado e impresion de las noticias de MASHABLE de news
  def mashable_show_new_news ()
    @number_new = 0
    @array = []
    @hash = mashable_api_request
    @contador = 0

    @hash["new"].each do |i|
      @contador = @contador + 1
      aux = Date.parse(i["post_date"])
      @number_new = @number_new + 1
      if @contador == 6
        puts "Pulse cualquier tecla para ver las siguientes 5 noticias".upcase.colorize(:color => :light_cyan)
        caracter = gets.chomp.to_i
        system("clear")
      end
      puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
      puts "NOTICIA NUMERO #{@number_new}: \n".colorize(:color => :light_cyan)
      puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{i['title']}\n".upcase
      puts " AUTOR: ".colorize(:color => :light_cyan)+"#{i['author']}\n".upcase
      puts " FECHA: ".colorize(:color => :light_cyan)+"#{aux.strftime("%d/%m/%Y")}\n".upcase
      puts "  LINK: ".colorize(:color => :light_cyan)+"#{i['link']}\n".upcase
      @array[@number_new]= i['link']
      puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)
    end

    puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
     @c = gets.chomp.to_i
      if @c == 0
        puts "saliendo"
      else
        Launchy.open("#{@array[@c]}")
      end

   true
  end

  #Organizacion, filtrado e impresion de las noticias de MASHABLE de RISING
  def mashable_show_rising_news ()
    @number_new = 0
    @array = []
    @hash = mashable_api_request
    @contador = 0

    @hash["rising"].each do |i|
      @contador = @contador + 1
      aux = Date.parse(i["post_date"])
      @number_new = @number_new + 1
      if @contador == 6
        puts "Pulse cualquier tecla para ver las siguientes 5 noticias".upcase.colorize(:color => :light_cyan)
        caracter = gets.chomp.to_i
        system("clear")
      end
      puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
      puts "NOTICIA NUMERO #{@number_new}: \n".colorize(:color => :light_cyan)
      puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{i['title']}\n".upcase
      puts " AUTOR: ".colorize(:color => :light_cyan)+"#{i['author']}\n".upcase
      puts " FECHA: ".colorize(:color => :light_cyan)+"#{aux.strftime("%d/%m/%Y")}\n".upcase
      puts "  LINK: ".colorize(:color => :light_cyan)+"#{i['link']}\n".upcase
      @array[@number_new]= i['link']
      puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)
    end

      puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
      @c = gets.chomp.to_i
      if @c == 0
        puts "saliendo"
      else
        Launchy.open("#{@array[@c]}")
      end

   true
  end

  #Organizacion, filtrado e impresion de las noticias de MASHABLE de HOT
  def mashable_show_hot_news ()
    @number_new = 0
    @array = []
    @hash = mashable_api_request
    @contador = 0

    @hash["hot"].each do |i|
      @contador = @contador + 1
      aux = Date.parse(i["post_date"])
      @number_new = @number_new + 1
      if @contador == 6
        puts "Pulse cualquier tecla para ver las siguientes 5 noticias".upcase.colorize(:color => :light_cyan)
        caracter = gets.chomp.to_i
        system("clear")
      end
      puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
      puts "NOTICIA NUMERO #{@number_new}: \n".colorize(:color => :light_cyan)
      puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{i['title']}\n".upcase
      puts " AUTOR: ".colorize(:color => :light_cyan)+"#{i['author']}\n".upcase
      puts " FECHA: ".colorize(:color => :light_cyan)+"#{aux.strftime("%d/%m/%Y")}\n".upcase
      puts "  LINK: ".colorize(:color => :light_cyan)+"#{i['link']}\n".upcase
      @array[@number_new]= i['link']
      puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)
    end

      puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
      @c = gets.chomp.to_i
      if @c == 0
        puts "saliendo"
      else
        Launchy.open("#{@array[@c]}")
      end

   true
  end
end
