require 'rubygems'
require 'rest_client'
require 'json'

class Reddit_fill_organize_matrix
    def reddit_api_request
        @url = 'https://www.reddit.com/.json'
              @response = RestClient.get(@url)
              @endpoint_reddit ='https://www.reddit.com'
        @hash = JSON.parse(@response.body)
      end

      def reduce_data_reddit
        @hash_reddit = fill_hash_reddit
        @array_1 = []
     
         @hash_reddit["data"]["children"].each_with_index do |n, i|
             @timestamp = n["data"]["created_utc"]
             @hour = Date.parse"#{Time.at(@timestamp)}"
             @hour_fixed = @hour.strftime("%d-%m-%Y")
             @link_fixed ="#{@endpoint_reddit}#{n["data"]["permalink"]}"
             @array_1[i] = []
             @array_1 [i][0] =  n['data']['title']
             @array_1 [i][1] =  @link_fixed
             @array_1 [i][2] =  n['data']['author']
             @array_1 [i][3] =  @hour_fixed
             end
           @array_1
         end

end