require 'rubygems'
require 'rest_client'
require 'json'

#clase propia de la pagina REDDIT
class Reddit
#Obtencion de la data del API de REDDIT
    def reddit_api_request
      @url = 'https://www.reddit.com/.json'
			@response = RestClient.get(@url)
			@endpoint_reddit ='https://www.reddit.com'
      @hash = JSON.parse(@response.body)
    end

		#Organizacion, filtrado e impresion de las noticias de REDDIT
		def reddit_show_news
					@number_new = 0
					@array = []
					@hash = reddit_api_request
					@contador = 0
					puts "                                           **********NOTICIAS ACTUALES DE REDDIT**********\n".colorize(:color => :light_cyan)

					@hash["data"]["children"].each do |i|
						@contador = @contador + 1
						@timestamp = i["data"]["created_utc"]
    				@hour = Date.parse"#{Time.at(@timestamp)}"
						@hour_fixed = @hour.strftime("%d-%m-%Y")
						@number_new = @number_new + 1
						if @contador == 6 || @contador == 11 || @contador == 16 || @contador == 21
							puts "Pulse cualquier tecla para ver las siguientes 5 noticias".upcase.colorize(:color => :light_cyan)
							caracter = gets.chomp.to_i
							system("clear")
						end
						puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
						puts "NOTICIA NUMERO #{@number_new}: \n".colorize(:light_cyan)
						puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{i['data']['title']}".upcase
						puts " AUTOR: ".colorize(:color => :light_cyan)+"#{i['data']['author']}".upcase
            puts " FECHA: ".colorize(:color => :light_cyan)+"#{@hour_fixed}".upcase
            puts "   URL: ".colorize(:color => :light_cyan)+"#{@endpoint_reddit}#{i["data"]["permalink"]}".upcase
						@array[@number_new]= "#{@endpoint_reddit}#{i['data']['permalink']}"
						puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)
					end

						puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
						@c = gets.chomp.to_i
						if @c == 0
						puts "SALIENDO"
							#menu ()
						else
    					Launchy.open("#{@array[@c]}")
						end

			return @array
		end
end
