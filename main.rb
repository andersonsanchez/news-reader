require_relative './reddit.rb'
require_relative './mashable.rb'
require_relative './digg.rb'
require_relative './all_websites_news.rb'
class Main
	def initialize ()
	end

		#creacion de instancias y llamado a los metodos de la clase mashable
	def get_single_key_mashable_menu

		c = read_char
		case c
			when "0"
				menu
			when "1"
				system ("clear")
				print "					CARGANDO NOTICIAS NUEVAS...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				request = Mashable.new()
				request.mashable_show_new_news
				print "\n"
			when "2"
				system ("clear")
				print "					CARGANDO NOTICIAS DE SUBIENDO...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				request = Mashable.new()
				request.mashable_show_rising_news
			when "3"
				system ("clear")
				print "					CARGANDO NOTICIAS DE TENDENCIA...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				request_mashable = Mashable.new()
				request_mashable.mashable_show_hot_news
			else
				system ("clear")
				print "\nSELECCIONÓ UNA OPCIÓN INVALIDA: #{c.inspect}\n\n ".colorize(:color => :light_cyan)
				sleep (2)
				mashable_menu
		end


	end

	#menu propio de mashable, categorizacion
	def mashable_menu
		system("clear")
		print "                                           **********NOTICIAS DE MASHABLE.COM**********\n".colorize(:color => :light_cyan)
		print "\nPor favor elija una de las siguientes categorias:\n".colorize(:color => :light_cyan)
		print "\n                 1- NUEVAS".colorize(:color => :light_cyan)
		print "                        2- SUBIENDO".colorize(:color => :light_cyan)
		print "\n\n                 3- TENDENCIA".colorize(:color => :light_cyan)
		print "                     0- VOLVER\n".colorize(:color => :light_cyan)
		get_single_key_mashable_menu
	end

	# leer un caracter sin presionar enter y sin imprimirlo por pantalla
	def read_char
		begin
			old_state = `stty -g`
			system "stty raw -echo"
			c = STDIN.getc.chr
			if(c=="\e")
				extra_thread = Thread.new{
					c = c + STDIN.getc.chr
					c = c + STDIN.getc.chr
				}
				extra_thread.join(0.00001)
				extra_thread.kill
			end
		rescue => ex
			puts "#{ex.class}: #{ex.message}"
			puts ex.backtrace
		ensure
			system "stty #{old_state}"
		end
	return c
	end

		#creacion de instancias y llamado a los metodos de las diferentes clases
	def get_single_key_main_menu
		c = read_char
		case c
			when "0"
				system("clear")
				print "					CERRANDO PROGRAMA... VUELVA PRONTO\n".colorize(:color => :light_cyan)
				sleep (2)
				system("exit")
				system ("clear")
			when "1"
				system ("clear")
				print "					CARGANDO NOTICIAS DE REDDIT...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				request_reddit = Reddit.new()
				request_reddit.reddit_show_news


			when "2"
				system ("clear")
				print "					CARGANDO NOTICIAS DE MASHABLE...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				mashable_menu
			when "3"
				system ("clear")
				print "					CARGANDO NOTICIAS DE DIGG...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				request_digg = Digg.new()
				request_digg.digg_show_news
			when "4"
				system ("clear")
				print "					CARGANDO NOTICIAS...".colorize(:color => :light_cyan)
				sleep (1)
				system ("clear")
				@array_all_news_by_date = []
				request_all_news_by_date = Newspaper.new()
				@array_all_news_by_date = request_all_news_by_date.print_data_array
				puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
				@c = gets.chomp.to_i
				if @c == 0
				menu()
				else
					Launchy.open("#{@array_all_news_by_date[@c-1][1]}")
				end

			else
				system ("clear")
				print "\nSELECCIONO UNA OPCIÓN INVALIDA: #{c.inspect}\n\n ".upcase.colorize(:color => :light_cyan)
				sleep (2)
				menu()
		end
	end

	#menu principal
	def menu ()
		system("clear")
		print "                            **********BIENVENIDO AL LECTOR DE NOTICIAS DE HACK**********\n".colorize(:light_cyan)
		print "\nPor favor elija una de las siguientes opciones:\n".upcase.colorize(:color => :light_cyan)
		print "\n 1- Ver las noticias actuales de Reddit.com".upcase.colorize(:color => :light_cyan)
		print "                         2- Ver las noticias actuales de mashable.com".upcase.colorize(:light_cyan)
		print "\n\n 3- Ver las noticias actuales de digg.com".upcase.colorize(:color => :light_cyan)
		print "                           4- Ver las todas las noticias actuales".upcase.colorize(:color => :light_cyan)
		print "\n\n                                                    0- SALIR\n".upcase.colorize(:color => :light_cyan)
		get_single_key_main_menu
	end
end

news_reader = Main.new()
news_reader.menu



