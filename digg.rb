require 'json'
require 'rest_client'
require 'rubygems'
require 'launchy'
require 'colorize'

#clase propia de la pagina Digg
class Digg

#Obtencion de la data del API de DIGG
    def digg_api_request
        @url = 'http://digg.com/api/news/popular.json'
        @response = RestClient.get(@url)
        @hash = JSON.parse(@response.body)
    end

#Organizacion, filtrado e impresion de las noticias de DIGG
    def digg_show_news
        @contador = 0
        @hash = digg_api_request
        @number_new = 0
        @array = []
        puts "                                           **********NOTICIAS ACTUALES DE DIGG**********\n".colorize(:color => :light_cyan)
        @hash["data"]["feed"].each do |i|
            @contador = @contador + 1
            @timestamp= i['date']
            @hour = Date.parse"#{Time.at(@timestamp)}"
			@hour_fixed = @hour.strftime("%d/%m/%Y")
            @number_new = @number_new + 1
            if @contador == 6
                puts "Pulse cualquier tecla para ver las siguientes 5 noticias".upcase.colorize(:color => :light_cyan)
                caracter = gets.chomp.to_i
                system("clear")
            end
            puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
            puts "NOTICIA NUMERO #{@number_new}: \n".colorize(:color => :light_cyan)
            puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{i['content']['title_alt']}\n".upcase
            puts " FECHA: ".colorize(:color => :light_cyan)+"#{@hour_fixed}".upcase
            puts "   URL: ".colorize(:color => :light_cyan)+"#{i['content']['original_url']}".upcase
            @array[@number_new]= i['content']['original_url']
            puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)
            end

        puts " Si desea abrir una noticia en el navegador web ingrese el numero de la noticia, 0 para volver".upcase.colorize(:color => :light_cyan)
        @c = gets.chomp.to_i
        if @c == 0
            puts "saliendo"
        else
            Launchy.open("#{@array[@c]}")
        end

    end

end
