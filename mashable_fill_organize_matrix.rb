require 'rubygems'
require 'rest_client'
require 'json'

class Mashable_fill_organize_matrix

    def fill_hash_mashable
        url = 'http://mashable.com/stories.json'
        response = RestClient.get(url)
        @hash_mashable = JSON.parse(response.body)
    end

    def reduce_data_mashable
        @hash_mashable = fill_hash_mashable
        
        @array_1 = []
        @hash_mashable["new"].each_with_index do |n, i|
            aux = Date.parse(n['post_date'])
            aux = aux.strftime("%d/%m/%Y")
            @array_1[i] = []
            @array_1[i][0] = n['title']
            @array_1[i][1] = n['link']
            @array_1[i][2] = n['author']
            @array_1[i][3] = aux
        end
        @array_1
    end

end