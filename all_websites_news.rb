require 'json'
require 'rest_client'
require 'rubygems'
require 'launchy'
require 'colorize'

#Declaracion de la clase Newspaper, obtencion de datos de las API, organizacion e impresion de las noticias de todas las paginas ordenadas por fecha.
class Newspaper

#Obtencion de la data del API de REDDIT
def fill_hash_reddit
    url = 'https://www.reddit.com/.json'
	response = RestClient.get(url)
	@endpoint_reddit ='https://www.reddit.com'
    @hash_reddit = JSON.parse(response.body)
end
#Obtencion de la data del API de DIGG

def fill_hash_digg
    url = 'http://digg.com/api/news/popular.json'
    response = RestClient.get(url)
    @hash_digg = JSON.parse(response.body)
end

#Obtencion de la data del API de MASHABLE
def fill_hash_mashable
    url = 'http://mashable.com/stories.json'
    response = RestClient.get(url)
    @hash_mashable = JSON.parse(response.body)
end

#Organizacion y filtrado de la data de mashable a solo lo necesario
def reduce_data_mashable
    @hash_mashable = fill_hash_mashable

    @array_1 = []
    @hash_mashable["new"].each_with_index do |n, i|
        aux = Date.parse(n['post_date'])
        aux = aux.strftime("%d/%m/%Y")
        @array_1[i] = []
        @array_1[i][0] = n['title']
        @array_1[i][1] = n['link']
        @array_1[i][2] = n['author']
        @array_1[i][3] = aux
    end
    @array_1
end

#Organizacion y filtrado de la data de digg a solo lo necesario
def reduce_data_digg
    @hash_digg = fill_hash_digg
    @array_1 = []
    @hash_digg["data"]["feed"].each_with_index do |n, i|
        @timestamp= n['date']
        @hour = Date.parse"#{Time.at(@timestamp)}"
        @hour_fixed = @hour.strftime("%d/%m/%Y")
        @array_1[i] = []
        @array_1[i][0] = n['content']['title_alt']
        @array_1[i][1] = n['content']['original_url']
        @array_1[i][2] = n['content']['author']
        @array_1[i][3] = @hour_fixed
        end
        @array_1
end

#Organizacion y filtrado de la data de reddit a solo lo necesario
def reduce_data_reddit
   @hash_reddit = fill_hash_reddit
   @array_1 = []

    @hash_reddit["data"]["children"].each_with_index do |n, i|
        @timestamp = n["data"]["created_utc"]
    	@hour = Date.parse"#{Time.at(@timestamp)}"
        @hour_fixed = @hour.strftime("%d-%m-%Y")
        @link_fixed ="#{@endpoint_reddit}#{n["data"]["permalink"]}"
        @array_1[i] = []
        @array_1 [i][0] =  n['data']['title']
        @array_1 [i][1] =  @link_fixed
        @array_1 [i][2] =  n['data']['author']
        @array_1 [i][3] =  @hour_fixed
        end
      @array_1
    end

    #Creacion de una matriz conjunta con todas las noticias ordenadas por fecha
    def create_data_array
        @array = []
        @arry_digg = []
        @array_mashable = []
        @array= reduce_data_reddit
        @array_digg = reduce_data_digg
        @array_mashable = reduce_data_mashable

        @array_digg.each_with_index do |n, i|
        @array <<  @array_digg[i]
        end

        @array_mashable.each_with_index do |n, i|
        @array <<  @array_mashable[i]
        end

        ( @array.sort_by! { |i|   i[3]  } ).reverse
        @array
         end

    #Impresion de la matriz final
    def print_data_array
    @array = create_data_array

        @array.each_with_index do |n, i|
            if i == 5 || i ==10 || i==15 || i ==20 || i ==25 || i ==30 || i ==35 || i ==40 || i ==45
                puts "Pulse cualquier tecla para ver las siguientes 5 noticias"
                caracter = gets.chomp.to_i
                system("clear")
            end
            puts '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - '.colorize(:color => :light_cyan)
            puts "NOTICIA NUMERO: #{i+1} \n".colorize(:color => :light_cyan)
            puts "TÍTULO: ".colorize(:color => :light_cyan)+"#{n[0]}".upcase
            puts "   URL: ".colorize(:color => :light_cyan)+"#{n[1]}".upcase
            puts " AUTOR: ".colorize(:color => :light_cyan)+"#{n[2]}".upcase
            puts " FECHA: ".colorize(:color => :light_cyan)+"#{n[3]}".upcase
            puts "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n".colorize(:color => :light_cyan)

            end
    @array
    end

end
