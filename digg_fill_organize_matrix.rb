require 'rubygems'
require 'rest_client'
require 'json'

class Digg_fill_organize_matrix
def fill_hash_digg
    url = 'http://digg.com/api/news/popular.json'
    response = RestClient.get(url)
    @hash_digg = JSON.parse(response.body)
end

def reduce_data_digg
    @hash_digg = fill_hash_digg
    @array_1 = []
    @hash_digg["data"]["feed"].each_with_index do |n, i|
        @timestamp= n['date']
        @hour = Date.parse"#{Time.at(@timestamp)}"
        @hour_fixed = @hour.strftime("%d/%m/%Y")
        @array_1[i] = []
        @array_1[i][0] = n['content']['title_alt']
        @array_1[i][1] = n['content']['original_url']
        @array_1[i][2] = n['content']['author']
        @array_1[i][3] = @hour_fixed
        end
        @array_1
end

end